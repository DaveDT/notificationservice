using Akka.Actor;
using Insight.Notification.Core.Model;
using Insight.Notification.Domain.Commentary;
using Insight.Notification.Domain.CorporateActions;
using Insight.Notification.Domain.InsightManager;
using Insight.Notification.Domain.Roadshow;
using Insight.Notification.Domain.Supervisory;
using Insight.Notification.Messaging;
using Insight.Notification.System;
using Insight.Notification.System.Commands;
using Insight.Notification.System.Events;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;

namespace Interest.Notification.Services
{
    class Program
  {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
    {
            Logger.Debug("Notification Service started");
            var appSettings = ConfigurationManager.AppSettings;
            var isCommentary = Convert.ToBoolean(appSettings["isCommentary"]);
            var isInsight = Convert.ToBoolean(appSettings["isInsight"]);
            var isCorporateActions = Convert.ToBoolean(appSettings["isCorporateAction"]);
            var isRoadShow = Convert.ToBoolean(appSettings["isRoadShow"]);
            var isSupervisory = Convert.ToBoolean(appSettings["isSupervisory"]);

            printWelcome();

                ActorSystem system = ActorSystem.Create("insight");   
                // create system message bus to send messages to all domains .
                var systemMessageBus = system.ActorOf<MessageBus<SystemBusCommand, SystemBusEvent>>("insightSystemMessagingBus");
                IActorRef sysManager = system.ActorOf(Props.Create(() => new SystemManager(systemMessageBus,"insight")), "insightSystemManager");
                var sysStartTask = sysManager.Ask(new Start());
               // create list of task to based on config
                List<Task> starttasks = new List<Task>();
                    //start RoadShow
                if (isRoadShow)
                {
                    IActorRef roadShowManager = system.ActorOf(Props.Create(() => new RoadShowManager(system,systemMessageBus)), "roadShowManager");
                    starttasks.Add(roadShowManager.Ask(new Start()));
                
                }
                    //start Commentary ?
                if (isCommentary)
                {
                    IActorRef commentaryManager = system.ActorOf(Props.Create(() => new CommentaryManager(system, systemMessageBus)), "commentaryManager");
                    starttasks.Add(commentaryManager.Ask(new Start()));
                }
                    // start Insight ?
                if (isInsight)
                {
                    IActorRef insightManager = system.ActorOf(Props.Create(() => new InsightManager(systemMessageBus)), "insightManager");
                    starttasks.Add(insightManager.Ask(new Start()));
                }
                    // start Corporate Actions ?
                if (isCorporateActions)
                {
                    IActorRef corporateActionsManager = system.ActorOf(Props.Create(() => new CorporateActionsManager(systemMessageBus)), "corporateActionsManager");
                    starttasks.Add(corporateActionsManager.Ask(new Start()));
                }
                    //start supervisory
                if (isSupervisory)
                {
                    IActorRef supervisoryManager = system.ActorOf(Props.Create(() => new SupervisoryManager(system, systemMessageBus)), "supervisoryManager");
                    starttasks.Add(supervisoryManager.Ask(new Start()));
                }
           
            Console.WriteLine("Insight notification system has started");


         Task.WaitAll(starttasks.ToArray());

         
      
      Console.Read();
            system.Dispose();
    }
        private static void printWelcome()
        {
            Console.WriteLine("**********************************************************************");
            Console.WriteLine("**********************************************************************");
            Console.WriteLine();
            Console.WriteLine("           WELCOME TO INSIGHT NOTIFICATIONs SERVICE                   ");
            Console.WriteLine();
            Console.WriteLine("**********************************************************************");
            Console.WriteLine("**********************************************************************");
            Console.WriteLine();
        }
    }
   
}
