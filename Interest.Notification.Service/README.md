﻿TODO:

1) Clean up generic MessageBus -Done
2) Infra Create Actors for listeners, pass messaging bus , decided to leave as infra as non actors.
3) Create logging - Done
4) Add confluent messaging - ?? 
5) Create dummy UI for consuming the -??
6) Create DSL for interest related to commentary -
7) Add AutoFac DI
8) Create Readme.md  - Done
9) build Persistance library -in progress
10) Integrate Persistance for Storing notifications and Interests - did for RoadShow - Done
11) Look for generic way to create domain actor based on meta data - In progress
12) Move all system based modeling to the Core , ??
13) Add AKKA mailbox to all processing messaging and bus
14) Enchance Interest Model - Done
15) refactor Interest namspace from interest to interests - Done
16) update Interest Manager with reference to Interest Registry

Future:
Dashboard Monitoring:
		System Monitoring HeartBeat Service to Service Status

		
tech topics:

frameworks:
	AKKA
	CQRS
	DDD

	RestQL
	GraphQL


Patterns:

	Actor / Model


Messaging:
	ActiveMQ
	Kafka
	MSMQ
	SignalR
	Websockets
