﻿namespace Insight.Notification.Messaging
{
    public interface IBusEvent
    {
        string EventId { get; }
        object Event { get; }
        string Domain { get; }
    }
}