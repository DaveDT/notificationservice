using Akka.Actor;

namespace Insight.Notification.Messaging 
{
    public class UnRegisterEventHandler
  {

    public string EventId { get; }
    public IActorRef Handler { get; }

    public UnRegisterEventHandler(string eventId, IActorRef handler)
    {
      EventId = eventId;
      Handler = handler;
    }
  }
}
