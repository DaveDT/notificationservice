﻿using Akka.Actor;
using Akka.Util.Internal;
using System.Collections.Generic;
using System.Linq;

namespace Insight.Notification.Messaging
{
    public class MessageBus<T, U> : ReceiveActor where T: IBusCommand where U: IBusEvent
    {
        private readonly IList<RegisterCommandHandler> commandHandlers = new List<RegisterCommandHandler>();
        private readonly IList<RegisterEventHandler> eventHandlers = new List<RegisterEventHandler>();

        public MessageBus()
        {
            Receive<RegisterCommandHandler>(x =>commandHandlers.Add(x));
            Receive<UnRegisterCommandHandler>(x => commandHandlers.Where(c => c.CommandId == x.CommandId
             && c.Handler == x.Handler).FirstOrDefault(i =>
            commandHandlers.Remove(i)));
            Receive<RegisterEventHandler>(x => eventHandlers.Add(x));
            Receive<UnRegisterEventHandler>(x => eventHandlers.Where(e => e.EventId== x.EventId
            && e.Handler == x.Handler).FirstOrDefault(i =>
           eventHandlers.Remove(i)));


            Receive<T>(x => 
            commandHandlers.Where(
                c => c.CommandId == x.CommandId)
                .ForEach(c => 
                c.Handler.Tell(x)));
            Receive<U>(x =>
           eventHandlers.Where(
               e => e.EventId == x.EventId)
               .ForEach(c =>
               c.Handler.Tell(x)));

        }

    }
}


