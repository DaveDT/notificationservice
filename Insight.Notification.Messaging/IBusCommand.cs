﻿namespace Insight.Notification.Messaging
{
    public interface IBusCommand
    {
        string CommandId { get; }
        object Command { get; }
        string Domain { get; }
    }
}
