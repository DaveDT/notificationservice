using Akka.Actor;

namespace Insight.Notification.Messaging
{
    public class RegisterEventHandler
  {

    public string EventId { get; }
    public IActorRef Handler { get; }

    public RegisterEventHandler(string eventId, IActorRef handler)
    {
      EventId = eventId;
      Handler = handler;
    }
  }
}
