using Akka.Actor;

namespace Insight.Notification.Messaging
{
    public class UnRegisterCommandHandler
  {
    public string CommandId { get; }
    public IActorRef Handler { get; }

    public UnRegisterCommandHandler(string commandId, IActorRef handler)
    {
      CommandId = commandId;
      Handler = handler;
    }
  }
}
