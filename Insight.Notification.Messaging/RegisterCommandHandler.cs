using Akka.Actor;

namespace Insight.Notification.Messaging
{
    public class RegisterCommandHandler
  {
    public string CommandId { get; }
    public IActorRef Handler { get; }

    public RegisterCommandHandler(string commandId, IActorRef handler)
    {
      CommandId = commandId;
      Handler = handler;
    }
  }
}
