using System.Collections.Generic;

namespace InterestNotification.Messaging
{
    public class GroupInterestCommand : Command
  {
    private Interest currentInterest;
    private List<Group> Interestgroups;
    public GroupInterestCommand(CommandAction act,  Interest interest, List<Group> groups) : base(act)
    {
      Interestgroups = groups;
      currentInterest = interest;
    }
    public Interest Interest { get { return currentInterest; } }
    public List<Group> groupsInterested { get { return Interestgroups; } }
  }
}
