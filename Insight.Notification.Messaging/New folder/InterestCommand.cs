using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterestNotification.Messaging
{
  public class InterestCommand:InterestBusMessage
  {
    public string CommandId { get; }
    public Command Command { get; }

    public InterestCommand(string commandId, Command command)
    {
      CommandId = commandId;
      Command = command;
    }

  }
}
