using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterestNotification.Messaging
{
  public class Interest
  {
    public int id { get; }
    public string Message { get; }
    public Interest(string message)
    {
      Message = message;
      id = int.MaxValue;
    }
  }

  //public class InterestedIn : Interest { public InterestedIn(string message) : base(message) { } }
  //public class NoLongerInterestedIn : Interest { public NoLongerInterestedIn(string message) : base(message) { } }
}
