using Akka.Actor;
using Akka.Util.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterestNotification.Messaging
{
  public class InterestMessagingBus : ReceiveActor
  {
    private readonly IList<RegisterCommandHandler<InterestBusMessage>> _commandHandlers = new List<RegisterCommandHandler<InterestBusMessage>> ();
    private readonly IList<RegisterEventHandler> _notificationInterests = new List<RegisterEventHandler>();

    public InterestMessagingBus()
    {
      Receive<RegisterCommandHandler<InterestBusMessage>>(x => _commandHandlers.Add(x));
      Receive<InterestCommand>(x =>_commandHandlers.Where(c => c.CommandId == x.CommandId).ForEach(c => c.Handler.Tell(x.Command)));

    }
  }
}
