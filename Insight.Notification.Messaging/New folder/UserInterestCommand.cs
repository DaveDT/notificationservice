using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterestNotification.Messaging
{
  public class UserInterestCommand : Command
  {

        private Interest currentInterest;
        private List<User> InterestedUsers;
        public UserInterestCommand(CommandAction act, Interest interest, List<User> users) : base(act)
        {
            InterestedUsers = users;
            currentInterest = interest;

        }
        public Interest Interest { get { return currentInterest; } }
        public List<User> groupsInterested { get { return InterestedUsers; } }
    }
}
