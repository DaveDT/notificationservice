﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.Notification.Interests.Model
{
    public class InterestedUsers
    {
        List<User> users;
        Interest interest;

        public InterestedUsers(List<User> users, Interest interst)
        {
            this.users = users;
            this.interest = interst;
        }

        public List<User> Users { get => users; set => users = value; }
        public Interest Interest { get => interest; set => interest = value; }
    }
}
