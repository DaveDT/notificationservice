﻿namespace Insight.Notification.Interests.Model
{
    public class User
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }
}
