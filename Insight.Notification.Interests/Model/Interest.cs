using System;
using System.Collections.Generic;

namespace Insight.Notification.Interests.Model
{
    public class Interest
    {
        public string id { get; }
        public string Category { get; }
        public DateTime Created { get; }
        public string Description { get; }
        public object Details { get; }
        public string Domain { get; }
        public string Subject { get; }
        // create objects for seasonal  in details
        public bool isSeasonal { get; }
        // create objects for to define interval in details 
        public bool isInterval;

        public List<string> Keywords;

        
        
    
    public Interest(string domain, string category, object details, string subject,string description, List<string> keywords )
    {
            Category = category;
            Created = DateTime.Now;
            Description = description;
            Details = details;
            Domain = domain;
            Subject = subject;
            Keywords = keywords;
            id = createId(domain, category, subject);
    }
        private string createId(string domain, string category, string subject)
        {
            string identifier = string.Empty;
            identifier = domain + "-" + category + "-" + subject;
            return identifier;
        }
  }
    
}
