﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.Notification.Interests.Model
{
    public class InterestedGroups
    {
        private List<Group> groups;
        private Interest interest;

        public InterestedGroups(List<Group> groups, Interest interest)
        {
            this.groups = groups;
            this.interest = interest;
        }

        public List<Group> Groups { get => groups; set => groups = value; }
        public Interest Interest { get => interest; set => interest = value; }
    }
}
