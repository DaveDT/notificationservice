﻿using Insight.Notification.Interests.Commands;

namespace Insight.Notification.Interests
{
    interface IInterestRegistry
    {
        string Domain();
        bool AddUserInterest(UserInterestCommand cmd);
        bool RemoveUserInterest(UserInterestCommand cmd);
        bool ClearUsersInterest(UserInterestCommand cmd);

        bool AddGroupInterest(GroupInterestCommand cmd);
        bool RemoveGroupInterest(GroupInterestCommand cmd);
        bool ClearGroupsInterest(GroupInterestCommand cmd);
    }
}
