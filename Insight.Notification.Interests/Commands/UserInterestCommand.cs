using Insight.Notification.Interests.Model;
using Insight.Notification.Messaging;

namespace Insight.Notification.Interests.Commands
{
    public class UserInterestCommand : IBusCommand
    {
        private InterestedUsers command;
        private string domain;
       
        public UserInterestCommand(InterestedUsers data, ActionType action, string domain)
        {
            Action = action;
            CommandId = "UserInterestCommand";
            this.command = data;
            this.domain = domain;

        }
        public ActionType Action { get; }
        public string CommandId { get; }
        public string Domain { get; }
        public object Command { get; }
    }
}