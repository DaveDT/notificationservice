using Insight.Notification.Interests.Commands;
using Insight.Notification.Interests.Model;
using Insight.Notification.Interests;
using System.Collections.Generic;
using Insight.Notification.Messaging;

namespace Insight.Notification.Interests.Commands
{
    public class GroupInterestCommand  : IBusCommand
  {
        private InterestedGroups command;
        
        public GroupInterestCommand(InterestedGroups data, ActionType action,string domain  )
        {
            
            CommandId = "GroupInterestCommand";
            command = data;
            Domain = domain;
            Action = action;

        }
        public ActionType Action { get; }
        public string CommandId
        {
            get;
        }

        public string Domain
        {
            get;
        }

        public object Command { get => command; }
        
    }
 
}
