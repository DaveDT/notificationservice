﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.Notification.Interests.Commands
{
    public enum ActionType
    {
        Add,
        Delete,
        Update
    }
}
