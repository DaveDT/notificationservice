﻿using Insight.Notification.Interests;
using Insight.Notification.Messaging;

namespace Insight.Notification.Interests.Commands
{
    public class InterestBusCommand : IBusCommand
    {
        public InterestBusCommand(object cmd, string cmdid, string domain)
        {
            Command = cmd;
            CommandId = cmdid;
            Domain = domain;
        }
        public InterestBusCommand(GroupInterestCommand grpcmd)
        {
            Command = grpcmd.Command;
            CommandId = grpcmd.CommandId;
            Domain = grpcmd.Domain;   
        }

        public InterestBusCommand(UserInterestCommand usercmd)
        {
            Command = usercmd.Command;
            CommandId = usercmd.CommandId;
            Domain = usercmd.Domain;
        }
        public string CommandId { get; }
        public string Domain{ get; }
        public object Command { get; }
    }
}
