﻿using Akka.Actor;
using Insight.Notification.Interests;
using Insight.Notification.Interests.Commands;
using Insight.Notification.Interests.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.Notification.Interests
{
    public class InterestRegistry : IInterestRegistry
    {
        
               private readonly IDictionary<string, InterestedUsers> userInterestRegistry = new Dictionary<string, InterestedUsers>();
               private readonly IDictionary<string, InterestedGroups> groupInterestRegistry = new Dictionary<string, InterestedGroups>();


        string domainName = string.Empty;
        public InterestRegistry(string domain)
        {
            domainName = domain;
        }

        //public void ProcessUserCommand(UserInterestCommand cmd)
        //{
        //    switch (cmd.Action) {
        //        case Commands.ActionType.Add:
        //            AddUserInterest(cmd);
        //            break;
        //    } 

        //}
        private string getInterestKey(Interest interest)
        {
            string key = interest.Domain +"-"+interest.Subject +"-" + interest.Category;
            return key;
        }
        public bool AddGroupInterest(GroupInterestCommand cmd)
        {
            InterestedGroups cmdData = cmd.Command as InterestedGroups;
            string key = getInterestKey(cmdData.Interest);

            return true;
        }

        public bool AddUserInterest(UserInterestCommand cmd)
        {
            InterestedUsers cmdData = cmd.Command as InterestedUsers;
            string key = getInterestKey(cmdData.Interest);

            return true;
        }

        public bool ClearGroupsInterest(GroupInterestCommand cmd)
        {
            throw new NotImplementedException();
        }

        public bool ClearUsersInterest(UserInterestCommand cmd)
        {
            throw new NotImplementedException();
        }

        public string Domain()
        {
            throw new NotImplementedException();
        }

        public bool RemoveGroupInterest(GroupInterestCommand cmd)
        {
            throw new NotImplementedException();
        }

        public bool RemoveUserInterest(UserInterestCommand cmd)
        {
            throw new NotImplementedException();
        }
    }
}
