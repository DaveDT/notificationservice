﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightNotification.Domain.RoadShow.Model
{
    public interface IRoadShowMeetingPayload
    {
        List<string> To { get; set; }
        String Status { get; set; }
        String Message { get; set; }

    }
}
