﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InsightNotification.Domain.RoadShow.Model
{
    public class RoadShowMeetingPayload : IRoadShowMeetingPayload
    {
        public List<string> To { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
    }

}
