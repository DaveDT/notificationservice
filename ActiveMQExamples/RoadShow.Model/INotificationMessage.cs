﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightNotification.Domain.RoadShow.Model
{
  
public interface INotificationMessage<T>
{
    string Id { get; }
    string Domain { get; }
    DateTime CreationTime { get; }
    T Payload { get; }
}
}