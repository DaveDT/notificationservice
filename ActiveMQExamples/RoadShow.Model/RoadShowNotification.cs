﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightNotification.Domain.RoadShow.Model
{
    public class RoadShowNotification<T> : INotificationMessage<T>
    {
        public RoadShowNotification(T Data)
        {
            Payload = Data;
        }
        public string Id => Guid.NewGuid().ToString();

        public string Domain => "Supervisory";

        public DateTime CreationTime => DateTime.Now;

        public T Payload { get; }
    }
}
