﻿using Insight.Notification.Interests;
using Insight.Notification.Messaging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.Notification.System.Events
{
    public class SystemBusEvent : IBusEvent
    {
        public SystemBusEvent(string eventId, object @event, string domain)
        {
            EventId = eventId;
            Event = @event;
            Domain = domain;
        }

        public string EventId { get; }

        public object Event { get; }

        public string Domain { get; }

    }
}
