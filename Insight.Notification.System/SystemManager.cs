﻿using Akka.Actor;
using Insight.Notification.Core.Model;
using Insight.Notification.Infra.Messaging.ActiveMQ;
using Insight.Notification.System.Commands;
using System;


namespace Insight.Notification.System
{
    public class SystemManager : ReceiveActor
    {
        private IActorRef systemMessageBus;
        private ActiveMQTopicListener sysMsgListener;
        string systemName;
     
        public SystemManager(IActorRef _systemMessageBus, string sysName)
        {
            systemMessageBus = _systemMessageBus;
            systemName = sysName;
            Receive<Start>(x =>
            {
                sysMsgListener = new ActiveMQTopicListener(sysName + "-system");
                sysMsgListener.DataPublisher += SysMsgListener_DataPublisher;
                Console.WriteLine(sysName + "-system manager has started");
                Sender.Tell("ok");
            });
            Receive<Stop>(x =>
            {
                Console.WriteLine("Receive Stop Message");
            });
        }

        private void SysMsgListener_DataPublisher(object sender, string e)
        {
            if (e == "RESET")
            {
                var reset = new ResetCommand("system");
                systemMessageBus.Tell(new SystemBusCommand(reset));
            }
        }
    }
}
