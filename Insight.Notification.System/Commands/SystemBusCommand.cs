﻿

using Insight.Notification.Interests;
using Insight.Notification.Messaging;
using Insight.Notification.System;

namespace Insight.Notification.System.Commands
{
    public class SystemBusCommand : IBusCommand
    {
       public SystemBusCommand(IBusCommand command)
        {
            Command = command.Command;
            CommandId = command.CommandId;
            Domain = command.Domain;

        }
        public string CommandId
        {
            get;
        }

        public string Domain
        {
            get;
        }

        public object Command
        {
            get;
        }
    }
}
