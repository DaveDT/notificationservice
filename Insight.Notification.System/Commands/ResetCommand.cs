﻿using Insight.Notification.Interests;
using Insight.Notification.Messaging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.Notification.System.Commands
{
    public class ResetCommand : IBusCommand
    {
        public ResetCommand(string domain)
        {
            Domain = domain;
         
        }
        public string CommandId => "ResetCommand";


        public string Domain { get; }

        public object Command => null;
    }
}
