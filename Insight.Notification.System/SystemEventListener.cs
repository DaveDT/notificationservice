﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.Notification.System
{
    /// <summary>
    /// Listen for specific event coming from system Topic
    /// </summary>
    public class SystemEventListener
    {
        private string _systemID;
        public SystemEventListener(string systemId)
        {
            _systemID = systemId;
        }

        public string SystemID { get => _systemID;  }
    }
}
