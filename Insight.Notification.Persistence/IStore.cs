using System.Collections.Generic;

namespace Insight.Notification.Persistence
{
    public interface IStore<T>
  {
    IEnumerable<T> get(string id);
    IEnumerable<T> add(T item);
    IEnumerable<T> exists(string id);

  }
}
