﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Notification.Infra.DB.MongoDB
{
    public interface IMongoRepository<T>
    {
        Task<IAsyncCursor<T>> GetAsync();
        Task<IAsyncCursor<T>> GetAsync(string id);
        Task InsertAsync(T item);
        Task<DeleteResult> DeleteAsync(string id);
    }
}
