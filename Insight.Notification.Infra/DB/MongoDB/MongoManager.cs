﻿using MongoDB.Driver;
namespace Insight.Notification.Infra.DB.MongoDB
{
    public class MongoManager<T>
    {
        private IMongoClient mongoClient;
        private IMongoDatabase mongoDB;
        public MongoManager(string connectionString,  string DbName, string collectionName)
        {
            mongoClient = new MongoClient(connectionString);
            mongoDB = mongoClient.GetDatabase(DbName);
            this.Values = mongoDB.GetCollection<T>(collectionName);
        }
       public IMongoCollection<T> Values { get; set; }
    }
            
        
}

