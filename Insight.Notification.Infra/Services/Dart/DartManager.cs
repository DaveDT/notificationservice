using Akka.Actor;
using Insight.Notification.Interests.Commands;
using Insight.Notification.Interests.Model;
using System;
using System.Collections.Generic;

namespace Insight.Notification.Infra.Services.Dart
{
    public class DartManager {
        private IActorRef interestMessagingBus;
        private string Domain;
    public DartManager(IDartConfigSettings config, IActorRef interestmessagebus, string domain)
    {
            interestMessagingBus = interestmessagebus;
            Domain = domain;
    }

        public bool reset()
        {
            throw new NotImplementedException();
        }

        public bool start()
        {
            simulateInterest();

            return true;
        }
        private void simulateInterest()
        {

            /* Create Group Interest */
            List<Group> interestedGroups = new List<Group>();
            Group g1 = new Group();
            g1.Name = "Insight Commentary G1";
            g1.Id = "comm_grp_id_001";
            g1.Type = "Ges";
            Group g2 = new Group();
            g2.Name = "Insight Commentary G2";
            g2.Id = "comm_grp_id_002";
            g2.Type = "Ges";
            //add to collection
            interestedGroups.Add(g1);
            interestedGroups.Add(g2);
            Interest mygroupInterest = new Interest(Domain, "Company", null, "XYZ", "group is interested in ZYX", null);
            InterestedGroups interestedGrp = new InterestedGroups(interestedGroups,mygroupInterest);

            /* Create USer Interest */
            Interest userInterest = new Interest(Domain, "COmpany", null, "XYZ", "user interest in ZYX", null);

            List<User> usersInteseted = new List<User>();
            usersInteseted.Add(new User
            {
                Id = "1234",
                Name = "Dave",
                Type = "BOFA"
            });
            InterestedUsers interestedUsers = new InterestedUsers(usersInteseted, userInterest);
            var grpcmd = new GroupInterestCommand(interestedGrp, ActionType.Add, Domain);
            var usercmd = new UserInterestCommand(interestedUsers, ActionType.Add, Domain);

            interestMessagingBus.Tell(new InterestBusCommand(grpcmd));
            interestMessagingBus.Tell(new InterestBusCommand(usercmd));
        }
        public bool stop()
        {
            throw new NotImplementedException();
        }

        public bool subscribeToGroupDomainInterests(IList<string> domains)
        {
            throw new NotImplementedException();
        }

        public bool subscribeToUserDomainInterests(IList<string> domains)
        {
            throw new NotImplementedException();
        }

        public bool unsubscribeToGroupDomainInterests(IList<string> domains)
        {
            throw new NotImplementedException();
        }

        public bool unsubscribeToUserDomainInterests(IList<string> domains)
        {
            throw new NotImplementedException();
        }
    }
}
