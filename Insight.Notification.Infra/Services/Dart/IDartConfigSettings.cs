﻿namespace Insight.Notification.Infra.Services.Dart
{
    public interface IDartConfigSettings
    {
        string connectionAddress();
        string user();
        string password();
    }
}