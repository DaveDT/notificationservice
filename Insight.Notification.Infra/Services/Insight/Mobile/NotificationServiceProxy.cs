﻿using Insight.Notification.Infra.Services.Insight.Mobile.Config;
using Insight.Notification.Infra.Services.Insight.Mobile.Model;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Notification.Infra.Services.Insight.Mobile
{
    public class NotificationServiceProxy
    {
                string appCode;
                string baseUri;
                string userPreferenceEndpoint;
                string sendNotificationEndpoint;
                string notificationStatusEndpoint;
                string userNotificationStatusEndpoint;
                string serviceKey;
        
        public NotificationServiceProxy(NotificationServiceConfig config)
        {
            if (config != null)
            {
                this.appCode = config.appCode;
                this.baseUri = config.baseUri;
                this.notificationStatusEndpoint = config.notificationStatusEndpoint;
                this.sendNotificationEndpoint = config.sendNotificationEndpoint;
                this.serviceKey = config.serviceKey;
                this.userPreferenceEndpoint = config.userPreferenceEndpoint;
                this.userNotificationStatusEndpoint = config.userNotificationStatusEndpoint;
            }
        }

        public async Task<AppPerferenceResponse> GetUserPreferencesAsync(string appcode)
        {
            
            AppPerferenceResponse response = null; 
            using( var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("serviceKey", this.serviceKey);
                client.DefaultRequestHeaders.Add("appCode", this.appCode);
                client.DefaultRequestHeaders.Add("Accept", "application/json");
                //Accept: application/json
                var url = this.baseUri + this.userPreferenceEndpoint + appcode;
                var uri = new Uri(url);

                try {
                    var result = await client.GetStringAsync(uri);
                    response= JsonConvert.DeserializeObject< AppPerferenceResponse>(result);
                   
                } catch(Exception ex)
                {
                    return null;
                }

            }
            return response;
        }
        public async Task<SendNotificationResponse> SendNotification(NotificationRequest msg)
        {

            SendNotificationResponse response = null;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("serviceKey", this.serviceKey);
                client.DefaultRequestHeaders.Add("appCode", this.appCode);
               
                var url = this.baseUri + this.sendNotificationEndpoint;
                var uri = new Uri(url);
                try
                {

                    var requestBody = JsonConvert.SerializeObject(msg);
                    var request = new StringContent(requestBody, Encoding.UTF8, "application/json");

                    var reqResponse = await client.PostAsync(uri, request);
                    if (reqResponse.IsSuccessStatusCode)
                    {
                        string result = await reqResponse.Content.ReadAsStringAsync();
                        response = JsonConvert.DeserializeObject<SendNotificationResponse>(result);
                        
                        return response;
 
                    }
                    else
                    {
                        
                        return null;
                    }

                }
                catch (Exception ex)
                {
                    return null;
                }

            }
            return response;
        }

        public async Task<NotificationStatusResponse> GetNotificationStatusById(string id)
        {
            NotificationStatusResponse response = null;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("serviceKey", this.serviceKey);
                client.DefaultRequestHeaders.Add("appCode", this.appCode);
                var url = this.baseUri + this.notificationStatusEndpoint + id;
                var uri = new Uri(url);
                try
                {
                    var result = await client.GetStringAsync(uri);
                    response = JsonConvert.DeserializeObject<NotificationStatusResponse>(result);
               }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return response;
        }
        public async Task<NotificationStatusResponse> GetNotificationStatusByIdForUser(string id, string nbkid)
        {
            NotificationStatusResponse response = null;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("serviceKey", this.serviceKey);
                client.DefaultRequestHeaders.Add("appCode", this.appCode);
                var url = this.baseUri + this.notificationStatusEndpoint + id;
                var uri = new Uri(url);
                try
                {
                    var result = await client.GetStringAsync(uri);
                    response = JsonConvert.DeserializeObject<NotificationStatusResponse>(result);
                }
                catch (Exception ex)
                {
                    return null;
                }

            }
            return response;
        }
    }
}
