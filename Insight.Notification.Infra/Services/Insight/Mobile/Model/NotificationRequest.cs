﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Notification.Infra.Services.Insight.Mobile.Model
{
    [Serializable]
    [DataContract]
    public class NotificationRequest
    {
        [DataMember]
        [BsonElement]
        public string title { get;set; }
        [DataMember]
        [BsonElement]
        public string description { get; set; }
        [DataMember]
        [BsonElement]
        public string expiryTS { get; set; }
        [DataMember]
        [BsonElement]
        public string appCode { get; set; }
        [DataMember]
        [BsonElement]
        public List<string> userId { get; set; }
        [DataMember]
        [BsonElement]
        public NotificationRequestDetails notificationMsg { get; set; }
    }
}
