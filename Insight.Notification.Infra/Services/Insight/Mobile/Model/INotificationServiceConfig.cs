﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Notification.Infra.Services.Insight.Mobile.Model
{
    public interface INotificationServiceConfig
    {
        string baseUri { get; set; }
        string userPreferenceEndpoint { get; set; }
        string sendNotificationEndpoint { get; set; }
        string notificationStatusEndpoint { get; set; }
        string userNotificationStatusEndpoint { get; set; }
        string serviceKey { get; set; }
        string appCode { get; set; }
    }
}
