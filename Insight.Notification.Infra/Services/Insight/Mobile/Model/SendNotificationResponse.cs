﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Notification.Infra.Services.Insight.Mobile.Model
{
    public class SendNotificationResponse
    {
        public string notificationId { get; set; }
        public string correlationId { get; set; }
        public string status { get; set; }
       
    }
}
