﻿using System.Collections.Generic;

namespace Insight.Notification.Infra.Services.Insight.Mobile.Model
{
    public class NotificationStatusResponse 
    {
        public string correlationId { get; set; }
        public string Status { get; set; }
        public List<NotificationStatusResponseDetails> response { get; set; }
    }
}
