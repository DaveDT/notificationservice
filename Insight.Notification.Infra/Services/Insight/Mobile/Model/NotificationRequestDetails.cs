﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Notification.Infra.Services.Insight.Mobile.Model
{
    public class NotificationRequestDetails 
    {
        [BsonElement]
        public string alert { get; set; }
        [BsonElement]
        public string path { get; set; }
    }
}
