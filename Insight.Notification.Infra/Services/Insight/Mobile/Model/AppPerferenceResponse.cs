﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Notification.Infra.Services.Insight.Mobile.Model
{
    public class AppPerferenceResponse 
    {
        public AppPerferenceResponse()
        {
        }

        public string status { get; set; }
        public string message { get; set; }
        public string correlationId { get; set; }
        public List<AppPerferenceResponseDetails> response { get; set; }
    }
    public class AppPerferenceResponseDetails 
    {
        public string userid { get; set; }
        public string ispilotuser { get; set; }
        public string cateogrycode { get; set; }
        public int categorylevel { get; set; }
        public string categorydesc { get; set; }
        public string appCode { get; set; }
    }
}
