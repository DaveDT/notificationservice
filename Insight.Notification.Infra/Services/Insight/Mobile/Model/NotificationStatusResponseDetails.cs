﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Notification.Infra.Services.Insight.Mobile.Model
{
    public class NotificationStatusResponseDetails
    {
        string deviceModel { get; set; }
        string userId { get; set; }
        long notificationId { get; set; }
        string notificationStatus { get; set; }
        string requestSubmitData { get; set; }
    }
}
