﻿using System.Collections.Specialized;
using System.Configuration;

namespace Insight.Notification.Infra.Services.Insight.Mobile.Config
{
    public class NotificationServiceConfig 
    {
        
        public NotificationServiceConfig(string configName)
        {
            var notificationSrcConfig = ConfigurationManager.GetSection( configName) as NameValueCollection;
            setNotificationServiceConfig(notificationSrcConfig["baseUri"], notificationSrcConfig["userPreferenceEndpoint"],
                    notificationSrcConfig["sendNotificationEndpoint"], notificationSrcConfig["notificationStatusEndpoint"], notificationSrcConfig["userNotificationStatusEndpoint"],
                    notificationSrcConfig["serviceKey"], notificationSrcConfig["appCode"]);
        }

        private void setNotificationServiceConfig(string baseUri, string userPreferenceEndpoint, string sendNotificationEndpoint, string notificationStatusEndpoint, string userNotificationStatusEndpoint, string serviceKey, string appCode)
        {
            this.baseUri = baseUri;
            this.userPreferenceEndpoint = userPreferenceEndpoint;
            this.sendNotificationEndpoint = sendNotificationEndpoint;
            this.notificationStatusEndpoint = notificationStatusEndpoint;
            this.userNotificationStatusEndpoint = userNotificationStatusEndpoint;
            this.serviceKey = serviceKey;
            this.appCode = appCode;
        }
        public string baseUri { get; set; }
        public string userPreferenceEndpoint { get; set; }
        public string sendNotificationEndpoint { get; set; }
        public string notificationStatusEndpoint { get; set; }
        public string userNotificationStatusEndpoint { get; set; }
        public string serviceKey { get; set; }
        public string appCode { get; set; }
    }
}
