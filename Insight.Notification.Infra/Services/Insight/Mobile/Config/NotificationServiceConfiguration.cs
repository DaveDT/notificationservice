﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
            
namespace Insight.Notification.Infra.Services.Insight.Mobile.Config
{
   
    public class DomainElement : ConfigurationElement
    {
        

        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string)base["name"]; }
        }
        [ConfigurationProperty("serviceKey", IsRequired = true)]
        public string ServiceKey
        {
            get { return (string)base["serviceKey"]; }
        }
        [ConfigurationProperty("appCode", IsRequired = true)]
        public string AppCode
        {
            get { return (string)base["appCode"]; }
        }
    }

    [ConfigurationCollection(typeof(DomainElement))]
    public class JobElementCollection : ConfigurationElementCollection
    {
        internal const string PropertyName = "domain";

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.BasicMapAlternate;
            }
        }
        protected override string ElementName
        {
            get
            {
                return PropertyName;
            }
        }

        protected override bool IsElementName(string elementName)
        {
            return elementName.Equals(PropertyName,
              StringComparison.InvariantCultureIgnoreCase);
        }

        public override bool IsReadOnly()
        {
            return false;
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new DomainElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((DomainElement)(element)).Name;
        }

        public DomainElement this[int idx]
        {
            get { return (DomainElement)BaseGet(idx); }
        }
    }

    public class DomainSection : ConfigurationSection
    {
        [ConfigurationProperty("domains")]
        public JobElementCollection Jobs
        {
            get { return ((JobElementCollection)(base["domains"])); }
            set { base["domains"] = value; }
        }
    }
}
