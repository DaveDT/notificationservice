﻿using Apache.NMS;
using System;

namespace Insight.Notification.Infra.Messaging.ActiveMQ
{
    public class ActiveMQTopicPublisher:IDisposable
    {

        IConnection connection = null;
        IMessageProducer producer = null;
        ISession session = null;
        public ActiveMQTopicPublisher(string topicName)
        {

        String user = env("ACTIVEMQ_USER", "admin");
        String password = env("ACTIVEMQ_PASSWORD", "password");
        String host = env("ACTIVEMQ_HOST", "localhost");
        int port = Int32.Parse(env("ACTIVEMQ_PORT", "61616"));
            String destination = topicName;
        int size = 256;

        String DATA = "abcdefghijklmnopqrstuvwxyz";
        String body = "";
	        for(int i=0; i<size; i ++) 
			{
	            body += DATA[i % DATA.Length];
	        }

    String brokerUri = "activemq:tcp://" + host + ":" + port;
    NMSConnectionFactory factory = new NMSConnectionFactory(brokerUri);

        connection = factory.CreateConnection(user, password);
            connection.Start();
	        session = connection.CreateSession(AcknowledgementMode.AutoAcknowledge);
    IDestination dest = session.GetTopic(destination);
    producer = session.CreateProducer(dest);
    producer.DeliveryMode = MsgDeliveryMode.NonPersistent;
	
	  //      for (int i=1; i <= messages; i ++) 
			//{
	  //          producer.Send(session.CreateTextMessage(body));
	  //          if ((i % 1000) == 0) 
			//	{
	  //              Console.WriteLine(String.Format("Sent {0} messages", i));
	  //          }
	  //      }
	
	        
	        
		}

      private static String env(String key, String defaultValue)
        {
            String rc = System.Environment.GetEnvironmentVariable(key);
            if (rc == null)
            {
                return defaultValue;
            }
            return rc;
        }
 public void publishMsg(string msg)
        {
            producer.Send(session.CreateTextMessage(msg));
        }


        public void Dispose()
        {
            connection.Close();
        }
    }


    }

