﻿using Apache.NMS;
using Apache.NMS.ActiveMQ;
using System;

namespace Insight.Notification.Infra.Messaging.ActiveMQ
{
    public class ActiveMQTopicListener:IDisposable
    {
        IConnection connection = null;
        public event EventHandler<string> DataPublisher;

        private void OnDataPublisher(string arg)
        {
            var handler = DataPublisher;
            if (handler != null)
                handler(this, arg);
        }
        public ActiveMQTopicListener(string topicName)

        {
            try
            {
                Console.WriteLine("Starting up ActiveMQ Listener for: "+topicName);

                String user = env("ACTIVEMQ_USER", "admin");
                String password = env("ACTIVEMQ_PASSWORD", "password");
                String host = env("ACTIVEMQ_HOST", "localhost");
                int port = Int32.Parse(env("ACTIVEMQ_PORT", "61616"));
                String destination = topicName;

                String brokerUri = "activemq:tcp://" + host + ":" + port + "?transport.useLogging=true";
                NMSConnectionFactory factory = new NMSConnectionFactory(brokerUri);

                connection = factory.CreateConnection(user, password);
                connection.Start();
                ISession session = connection.CreateSession(AcknowledgementMode.AutoAcknowledge);
                IDestination dest = session.GetTopic(destination);

                IMessageConsumer consumer = session.CreateConsumer(dest);
                DateTime start = DateTime.Now;
        

                Console.WriteLine("ActiveMQ Listner is Waiting for messages on topic: " + topicName);
                consumer.Listener += Consumer_Listener;

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            

        }

        private void Consumer_Listener(IMessage msg)
        {
            if (msg is ITextMessage)
            {
                ITextMessage txtMsg = msg as ITextMessage;
                String body = txtMsg.Text;
                OnDataPublisher(body);
            }
        }

        public void Dispose()
        {
            Console.WriteLine("Shutting down Listener.");
            connection.Close();
        }
        private static String env(String key, String defaultValue)
        {
            String rc = System.Environment.GetEnvironmentVariable(key);
            if (rc == null)
            {
                return defaultValue;
            }
            return rc;
        }

        private static String arg(String[] args, int index, String defaultValue)
        {
            if (index < args.Length)
            {
                return args[index];
            }
            return defaultValue;
        }
    }
}
