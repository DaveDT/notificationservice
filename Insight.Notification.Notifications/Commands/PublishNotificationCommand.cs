﻿using Insight.Notification.Messaging;

namespace Insight.Notification.Notifications.Commands
{
    public class PublishNotificationCommand : IBusCommand
    {
        private string commandId = "PublishNotificationCommand";
        private object command;
        private string domain;
        public PublishNotificationCommand(object command, string domain)
        {
            this.command = command;
            this.domain = domain;
        }
        public string CommandId { get { return commandId; } }

        public object Command { get { return command; } }

        public string Domain { get { return domain; } }
    }
}
