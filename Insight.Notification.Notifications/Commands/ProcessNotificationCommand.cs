﻿using Insight.Notification.Messaging;

namespace Insight.Notification.Notifications.Commands
{
    public class ProcessNotificationCommand : IBusCommand
    {
        private string commandId = "ProcessNotificationCommand";
        private object command;
        private string domain;
        public ProcessNotificationCommand(object _command, string _domain)
        {
            command = _command;
            domain = _domain;
        }
        public string CommandId { get { return commandId; } }

        public object Command { get { return command; } }

        public string Domain { get { return domain; } }
    }
}
