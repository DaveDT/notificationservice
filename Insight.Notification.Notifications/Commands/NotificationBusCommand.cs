﻿using Insight.Notification.Messaging;

namespace Insight.Notification.Notifications.Commands
{
    public class NotificationBusCommand : IBusCommand
    {
        public NotificationBusCommand(IBusCommand command)
        {
            Command = command.Command;
            CommandId = command.CommandId;
            Domain = command.Domain;
        }
        public string CommandId
        {
            get;
        }

        public string Domain
        {
            get;
        }

        public object Command
        {
            get;
        }
    }





}
