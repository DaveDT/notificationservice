﻿using Insight.Notification.Messaging;

namespace Insight.Notification.Notifications.Events
{
    public class SentNotificationEvent : IBusEvent
    {
        /// <summary>
        /// Event for Notifications being sent 
        /// </summary>
        /// <param name="event">data for response or Ack of notification being sent </param>
        /// <param name="domain">Lob of the source of the notifciaton was being sent</param>
        public SentNotificationEvent(object @event, string domain)
        {
              EventId = "SentNotificationEvent";
              Event = @event;
              Domain = domain;
        }

        public string EventId { get; }

        public object Event { get; }

        public string Domain { get; }
    }
}
