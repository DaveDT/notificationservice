﻿using Insight.Notification.Messaging;

namespace Insight.Notification.Notifications.Events
{
    public class NotificationBusEvent : IBusEvent
    {
        /// <summary>
        /// Wrapper Class to sent messages to message bus that is register for notifications
        /// </summary>
        /// <param name="eventId">eventId of event beding sent</param>
        /// <param name="event">data being sent on messageBus</param>
        /// <param name="domain">Lob of source of the event</param>
        public NotificationBusEvent(IBusEvent busEvent)
        {
            EventId = busEvent.EventId;
            Event = busEvent.Event;
            Domain = busEvent.Domain;
        }

        public string EventId { get; }

        public object Event { get; }

        public string Domain { get; }
    }
}
