﻿using Insight.Notification.Messaging;

namespace Insight.Notification.Notifications.Events
{
    public class ReceiveNotificationEvent : IBusEvent
    {

        /// <summary>
        /// Used to reister listeners to events that are happening in the service
        /// </summary>
        /// <param name="event">object or msg that you want to send</param>
        /// <param name="domain">Lob of the source of the data</param>
        public ReceiveNotificationEvent( object @event, string domain)
        {
            EventId = "ReceiveNotificationEvent";
            Event = @event;
            Domain = domain;
        }

        public string EventId { get; }
        public object Event { get; }
        public string Domain { get; }
    }
}
