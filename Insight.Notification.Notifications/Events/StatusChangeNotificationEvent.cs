﻿using Insight.Notification.Messaging;

namespace Insight.Notification.Notifications.Events
{
    public class StatusChangeNotificationEvent : IBusEvent
    {
        /// <summary>
        /// Event for Notifications being sent 
        /// </summary>
        /// <param name="event">data for status of notification being sent </param>
        /// <param name="domain">Lob of the source of the notifciaton was being sent</param>
        public StatusChangeNotificationEvent(object @event, string domain)
        {
            EventId = "StatusChangeNotificationEvent";
            Event = @event;
            Domain = domain;
        }

        public string EventId { get; }

        public object Event { get; }

        public string Domain { get; }
    }

}
