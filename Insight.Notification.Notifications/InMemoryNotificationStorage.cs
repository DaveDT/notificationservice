﻿using Insight.Notification.Notifications.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.Notification.Notifications
{
    public class InMemoryNotificationStorage<T>
    {
        private List<T> items;

        public InMemoryNotificationStorage()
        {
            items = new List<T>();
        }

        public InMemoryNotificationStorage(List<T> items)
        {
            this.items = items;
        }

        public virtual IEnumerable<T> GetNotifcations()
        {
            return items;
        }

        public virtual IEnumerable<T> GetNotifcationsById(string Id)
        {
            return items;
        }

        public virtual bool Remove(T item)
        {
            try
            {
                items.Remove(item);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public virtual bool Save(T item)
        {
            items.Add(item);
            return true;
        }

        public virtual bool Update(T item)
        {
            throw new NotImplementedException();
        }
    }
}
