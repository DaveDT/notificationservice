﻿using System;

namespace Insight.Notification.Notifications.Interfaces
{
    public interface INotificationMessage
    {
        string Id { get; }
        string Domain { get; }
        DateTime CreationTime { get; }
        object Payload { get; }
    }
}
