﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.Notification.Notifications.Interfaces
{
   public interface INotificationStorage<T>
    {
        IEnumerable<INotificationStorage<T>> GetNotifcations();
        IEnumerable<INotificationStorage<T>> GetNotifcationsById(string Id);
        bool Save(INotificationStorage<T> item);
        bool Update(INotificationStorage<T> item);
        bool Remove(INotificationStorage<T> item);
    }
}
