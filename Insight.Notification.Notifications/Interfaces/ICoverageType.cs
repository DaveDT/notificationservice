﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Insight.Notification.Notifications.Interfaces
{
    public interface ICoverageType
    {
        string Type { get; set; }
        List<string> SendTo { get; set; }

    }
}
