﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Web;

namespace InsightMobile.Behavior
{
    public class EnableCrossOriginResource : BehaviorExtensionElement, IEndpointBehavior
    {
        public override Type BehaviorType { get { return typeof(EnableCrossOriginResource); } }

        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
            
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            var reqHeaders = new Dictionary<string, string>();
            reqHeaders.Add("Access-Control-Allow-Origin", "*");
            reqHeaders.Add("Access-Control-Request-Method", "POST,GET,PUT,DELETE,OPTIONS");
            reqHeaders.Add("Access-Control_Allow-Headers", "Accept,Content-Type,Origin,serviceKey,appCode");
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new CustomHeaderMessageInspector(reqHeaders));
        }

        public void Validate(ServiceEndpoint endpoint)
        {
            
        }

        protected override object CreateBehavior()
        {
            return new EnableCrossOriginResource();
        }
    }
}