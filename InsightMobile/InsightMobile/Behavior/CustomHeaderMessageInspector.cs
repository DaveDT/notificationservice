﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;

namespace InsightMobile.Behavior
{
    internal class CustomHeaderMessageInspector : IDispatchMessageInspector
    {
        private Dictionary<string, string> reqHeaders;

        public CustomHeaderMessageInspector(Dictionary<string, string> reqHeaders)
        {
            this.reqHeaders = reqHeaders;
        }

        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            return null;
        }

        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            HttpResponseMessageProperty httpHeader;
            if (reply.Properties != null && reply.Properties.ContainsKey(HttpResponseMessageProperty.Name))
                httpHeader = reply.Properties[HttpResponseMessageProperty.Name] as HttpResponseMessageProperty;
            else
            {
                httpHeader = new HttpResponseMessageProperty { StatusCode = System.Net.HttpStatusCode.OK };
                reply.Properties.Add(HttpResponseMessageProperty.Name, httpHeader);
            }
            foreach(var item in this.reqHeaders)
            {
                httpHeader.Headers.Add(item.Key, item.Value);
            }
            try
            {
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.CacheControl, "private,no-cache");
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.Pragma, "no-cache");
                WebOperationContext.Current.OutgoingResponse.Headers.Add(HttpResponseHeader.Expires, "-1");
            }catch(Exception ex)
            {

            }
        }
    }
}