﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace InsightMobile
{
    [Serializable]
    [DataContract]
    public class NotificationRequest
    {
        [DataMember]
        public string title { get;set; }
        [DataMember]
        public string description { get; set; }
        [DataMember]
        public string expiryTS { get; set; }
        [DataMember]
        public string appCode { get; set; }
        [DataMember]
        public List<string> userId { get; set; }
        [DataMember]
        public NotificationMessageDetails notificationMsg { get; set; }
    }
}
