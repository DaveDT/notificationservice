﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightMobile
{
    public class SendNotificationResponse : ISendNotificationResponse
    {
        public string notificationId { get; set; }
        public string correlationId { get; set; }
        public string status { get; set; }
    }
}
