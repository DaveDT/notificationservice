﻿using System.Collections.Generic;

namespace InsightMobile
{
    public class NotificationStatusResponse : INotificationStatusResponse
    {
        public string correlationId { get; set; }
        public string Status { get; set; }
        public List<INotificationStatusResponseDetails> response { get; set; }
    }
}
