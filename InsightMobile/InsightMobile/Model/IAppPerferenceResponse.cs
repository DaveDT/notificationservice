﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightMobile
{

       public interface IAppPerferenceResponse
    {
        string status { get; set; }
        string message { get; set; }
        string correlationId { get; set; }

        IList<AppPerferenceResponseDetails> response { get; set; }
    }

    public interface IAppPerferenceResponseDetails
    {
        string userid { get; set; }
        string ispilotuser { get; set; }
        string cateogrycode { get; set; }
        int categorylevel { get; set; }
        string categorydesc { get; set; }
        string appCode { get; set; }


    }
}
