﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightMobile
{
    public interface INotificationStatusResponse
    {
        string correlationId { get; set; }
        string Status { get; set; }
        List<INotificationStatusResponseDetails> response { get; set; }
    }
    public interface INotificationStatusResponseDetails
    {
        string deviceModel { get; set; }
        string userId { get; set; }
        long notificationId { get; set; }
        string notificationStatus { get; set; }
        string requestSubmitData { get; set; }
    }
}
