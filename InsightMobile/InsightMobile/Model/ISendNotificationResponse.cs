﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightMobile
{
    public interface ISendNotificationResponse
    {
        string notificationId { get; set; }
        string correlationId { get; set; }
        string status { get; set; }
        
    }
    
}
