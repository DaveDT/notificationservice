﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightMobile
{
    public class NotificationMessageDetails
    {
        string alert { get; set; }
        string path { get; set; }

    }

    //public interface INotificationMessage
    //{
    //    string title { get; set; }
    //    string description { get; set; }
    //    string expiryTS { get; set; }
    //    string appCode { get; set; }
    //    IList<string> userId { get; set; }
    //    INotificationMessageDetails notificationMsg { get; set; }
    //}
}
