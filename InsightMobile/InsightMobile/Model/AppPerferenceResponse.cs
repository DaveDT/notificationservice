﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsightMobile
{
    public class AppPerferenceResponse : IAppPerferenceResponse
    {
        public AppPerferenceResponse()
        {
        }

        public string status { get; set; }
        public string message { get; set; }
        public string correlationId { get; set; }
        public IList<AppPerferenceResponseDetails> response { get; set; }
    }
    public class AppPerferenceResponseDetails : IAppPerferenceResponseDetails
    {
        public string userid { get; set; }
        public string ispilotuser { get; set; }
        public string cateogrycode { get; set; }
        public int categorylevel { get; set; }
        public string categorydesc { get; set; }
        public string appCode { get; set; }
    }
}
