﻿using System;
using System.Collections.Generic;

namespace InsightMobile
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NotificationService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select NotificationService.svc or NotificationService.svc.cs at the Solution Explorer and start debugging.
    public class NotificationService : INotificationService
    {
        

        public AppPerferenceResponse GetAppPreferences(string appcode)
        {
            AppPerferenceResponse response = new AppPerferenceResponse();
            response.message = "response from Test Mobile service";
            response.status = "SUCCESS";
            response.response = new List<AppPerferenceResponseDetails>();
            var details = new AppPerferenceResponseDetails();
            details.appCode = appcode;
            details.userid = "zkxtest";
            details.ispilotuser = "false";
            details.cateogrycode = "a";
            details.categorylevel = -1;
            response.response.Add(details);

            return response;
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public string GetNotificationStatus(string notificationid)
        {
            return "status for: " + notificationid;
        }

        public string GetUserNotificationStatus(string notificationid, string userid)
        {
            throw new NotImplementedException();
        }

        public SendNotificationResponse SendNotification(NotificationRequest req)
        {
            var request = req;
            var response = new SendNotificationResponse();
            // test use correlationID to return msg
            response.correlationId = req.description;
            response.notificationId = Guid.NewGuid().ToString();  
            response.status = "Success";
            return response;
        }
      
    }
}
