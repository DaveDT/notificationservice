﻿using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace InsightMobile
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INotificationService" in both code and config file together.
    [ServiceContract]
    public interface INotificationService
    {
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json,UriTemplate = @"/notification/v1/preferences/application/{appcode}")]
        AppPerferenceResponse GetAppPreferences(string appcode);

        [OperationContract]
        [WebGet(UriTemplate = @"/notification/v1/status/{notificationid}")]
        string GetNotificationStatus(string notificationid);


        [OperationContract]
        [WebGet(UriTemplate = @"/notification/v1/status/{notificationid}/{userid}")]
        string GetUserNotificationStatus(string notificationid, string userid);


        [OperationContract]
        [WebInvoke(ResponseFormat =WebMessageFormat.Json, RequestFormat =WebMessageFormat.Json,  BodyStyle=WebMessageBodyStyle.Bare,
            Method ="POST", UriTemplate =@"/notification/v1/send/")]
        SendNotificationResponse SendNotification(NotificationRequest request);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
