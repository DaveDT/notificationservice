﻿using Akka.Actor;
using Insight.Notification.Core.Model;
using Insight.Notification.Infra.Messaging.ActiveMQ;
using Insight.Notification.Infra.Services.Dart;
using Insight.Notification.Interests;
using Insight.Notification.Interests.Commands;
using Insight.Notification.Interests.Events;
using Insight.Notification.Messaging;
using Insight.Notification.Notifications.Commands;
using Insight.Notification.Notifications.Events;
using Insight.Notification.System.Commands;
using System;
using System.Collections.Specialized;
using System.Configuration;

namespace Insight.Notification.Domain.Commentary
{
    /// <summary>
    /// Manager Commentary notification and interests 
    /// </summary>
    public class CommentaryManager : ReceiveActor
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        DartManager DartService = null;
        IActorRef notificationMessageBus = null;
        IActorRef interestMessagingBus = null;
        IActorRef systemMessagingBus = null;
        private string Domain = SystemDomains.Commentary;

        public CommentaryManager(ActorSystem system, IActorRef systemMessageBus)
        {

            systemMessagingBus = systemMessageBus;
            InterestRegistry commenataryInterestRegistry = new InterestRegistry(Domain);
            // change to match actual Dart 
            var DartInterestService = ConfigurationManager.GetSection("DartInterestService") as NameValueCollection;
            Receive<Start>(x =>
            {
                Logger.Info("Commentary Manager Starting, creating messaging bus and registering commands and interests");
                Console.WriteLine("Register Group and User Interest to commentary interest Bus!! ");
                // create message bus
                notificationMessageBus = system.ActorOf<MessageBus<NotificationBusCommand, NotificationBusEvent>>("commentary.notification.MessagingBus");
                interestMessagingBus = system.ActorOf<MessageBus<InterestBusCommand, InterestBusEvent>>("commentary.interest.MessagingBus");
                // Register Commands and handlers
                systemMessagingBus.Tell(new RegisterCommandHandler("ResetCommand", Self));
                interestMessagingBus.Tell(new RegisterCommandHandler("GroupInterestCommand", Self));
                interestMessagingBus.Tell(new RegisterCommandHandler("UserInterestCommand", Self));
                interestMessagingBus.Tell(new RegisterCommandHandler("InterestBusCommand", Self));
                // Create instance for Dart service
                DartService = new DartManager(null, interestMessagingBus, Domain);
                //create instance of ActiveMQ to listen for notifications
                var notificationListener = new ActiveMQTopicListener("commentary.notifications");
                notificationListener.DataPublisher += NotificationListener_DataPublisher;
                // start Dart service   
                DartService.start();
                /* Todo: */
                // update dartManager to be Akka start Task
                // create actor for: Queue<NotificationMessage> 
                // send response for Task waiting            
                Sender.Tell("ok");
            });
            Receive<SystemBusCommand>(x =>
            {
                if (x.CommandId == "ResetCommand")
                    Console.WriteLine("Commentary Manager Received ResetCommand");
            });
            Receive<InterestBusCommand>(x =>
            {
                Console.WriteLine("Commentary Manager Received " + x.CommandId);
            });
            
        }
        /// <summary>
        /// Event handler for commentary notifications, generated from += DataPublisher of ActiveMQ instance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NotificationListener_DataPublisher(object sender, string e)
        {
                Console.WriteLine("Received notification: " + e);
                notificationMessageBus.Tell(new NotificationBusCommand(new ProcessNotificationCommand(e, "Commentary")));
        }  
    }
}

