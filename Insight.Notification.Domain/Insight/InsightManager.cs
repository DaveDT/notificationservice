using Akka.Actor;
using Insight.Notification.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Notification.Domain.InsightManager
{
    public class InsightManager : ReceiveActor
    {

        public InsightManager(IActorRef systemMessageBus)
        {
            Receive<Start>(x =>
            {
                Console.WriteLine("Insight Manager Started!");
                // 1) register for system message reset !
                // 2) create insight notification message bus

                Sender.Tell("ok");
            });
        }
    }
}