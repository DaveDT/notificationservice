using Insight.Notification.Core.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Akka.Actor;

namespace Insight.Notification.Domain.CorporateActions
{
  public class CorporateActionsManager : ReceiveActor
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public CorporateActionsManager(IActorRef systemMessageBus)
        {
            Receive<Start>(x =>
            {
                Logger.Info("Corporate Actions Manager Started!");
                Console.WriteLine("Corporate Actions Manager Started!");
                // 1) register for system message reset !
                // 2) create insight notification message bus

                Sender.Tell("ok");
            });
        }

    }
}
