﻿using Insight.Notification.Domain.Supervisory.Model;
using Insight.Notification.Infra.DB.MongoDB;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Threading;
using System.Threading.Tasks;

namespace Insight.Notification.Domain.Supervisory.Persistence
{
    public class SupervisoryMongoDB : IMongoRepository<SupervisoryMobileNotificationMapper>
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private MongoManager<SupervisoryMobileNotificationMapper> MongoStorage;
        public SupervisoryMongoDB(string connectionString, string DbName, string collectionName)
        {
            Logger.Info("Create Mongo Manger connectionstring: {0} , DBName: {1}, collectionName: {2} ", connectionString, DbName, collectionName);
            MongoStorage = new MongoManager<SupervisoryMobileNotificationMapper>(connectionString, DbName, collectionName);
        }

        public async Task<DeleteResult> DeleteAsync(string id)
        {
            var filter = Builders<SupervisoryMobileNotificationMapper>.Filter.Where(x => x.SrcReqId == id);
            CancellationToken t = new CancellationToken();
            var result = await this.MongoStorage.Values.DeleteOneAsync(filter);
            return result;
        }

        public async Task<IAsyncCursor<SupervisoryMobileNotificationMapper>> GetAsync()
        {
            var result = await this.MongoStorage.Values.FindAsync<SupervisoryMobileNotificationMapper>(null);

            return result;
        }

        public async Task<IAsyncCursor<SupervisoryMobileNotificationMapper>> GetAsync(string id)
        {

            var filter = new BsonDocument("SrcReqId ", id);

            var result = await this.MongoStorage.Values.FindAsync<SupervisoryMobileNotificationMapper>(filter);

            return result;

        }

        public async Task InsertAsync(SupervisoryMobileNotificationMapper item)
        {
            Logger.Info("Inserting new item into Mongo ReqId: {0} ", item.SrcReqId);
            await this.MongoStorage.Values.InsertOneAsync(item);
        }
    }
}
