﻿using Akka.Actor;
using Insight.Notification.Core.Model;
using Insight.Notification.Domain.Supervisory.Notifications;
using Insight.Notification.Messaging;
using Insight.Notification.System.Commands;
using System;
using System.Threading.Tasks;

namespace Insight.Notification.Domain.Supervisory
{
    public class SupervisoryManager : ReceiveActor
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private ActorSystem system;
        private IActorRef systemMessageBus, notificationManager;
        private string Domain = SystemDomains.Supervisory;
        public SupervisoryManager(ActorSystem system, IActorRef systemMessageBus)
        {
            this.system = system;
            this.systemMessageBus = systemMessageBus;
            Receive<Start>(x =>
            {
                Logger.Info("Supervisory Manager Started ");
                Console.WriteLine("Supervisory Manager Started!");

                systemMessageBus.Tell(new RegisterCommandHandler("ResetCommand", Self));
                this.notificationManager = system.ActorOf(Props.Create(() => new SupervisoryNotificationManager(system, 
                    SystemDomains.Supervisory)), "supervisoryNotificationManager");
                var t0 = notificationManager.Ask(new Start());
                Task.WaitAll(t0);
                Sender.Tell("ok");
            });
            Receive<SystemBusCommand>(x =>
            {
                if (x.CommandId == "ResetCommand")
                    Console.WriteLine("Supervisory Manager Received ResetCommand");
            });

        }
    }
}
