﻿using Akka.Actor;
using Insight.Notification.Core.Model;
using Insight.Notification.Domain.Supervisory.Model;
using Insight.Notification.Domain.Supervisory.Persistence;
using Insight.Notification.Infra.Messaging.ActiveMQ;
using Insight.Notification.Messaging;
using Insight.Notification.Notifications.Commands;
using Insight.Notification.Notifications.Events;
using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Threading.Tasks;

namespace Insight.Notification.Domain.Supervisory.Notifications
{
    public class SupervisoryNotificationManager : ReceiveActor
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private IActorRef notificationProcessor, notificationPublisher, notificationMessageBus;
        private SupervisoryMongoDB SupervisoryMongoStorage;
        private ActiveMQTopicListener notificationMsgListener;
        private ActorSystem system;
        private string Domain = SystemDomains.RoadShow;
        public SupervisoryNotificationManager(ActorSystem system, string domain)
        {
            var mongoConfig = ConfigurationManager.GetSection("InsightNotificationsMongoDB") as NameValueCollection;
            var mongoConnctionString = mongoConfig["connectionString"];
            var DbName = mongoConfig["dbName"];
            this.SupervisoryMongoStorage = new SupervisoryMongoDB(mongoConnctionString, DbName, domain);
            this.system = system;
            Receive<Start>(x =>
            {
                this.notificationMessageBus = system.ActorOf<MessageBus<NotificationBusCommand, NotificationBusEvent>>("supervisoryNotificationMessagingBus");
                this.notificationProcessor = system.ActorOf(Props.Create(() => new SupervisoryNotificationProcessor(notificationMessageBus)), "supervisoryNotificationProcessor");
                this.notificationPublisher = system.ActorOf(Props.Create(() => new SupervisoryNotificationPublisher(notificationMessageBus)), "supervisoryNotificationPublisher");
                var t1 = this.notificationProcessor.Ask(new Start());
                var t2 = this.notificationPublisher.Ask(new Start());
                Task.WaitAll(t1, t2);
                    // register events
                notificationMessageBus.Tell(new RegisterEventHandler("SentNotificationEvent", Self));
                notificationMessageBus.Tell(new RegisterEventHandler("StatusChangeNotificationEvent", Self));
                notificationMsgListener = new ActiveMQTopicListener("supervisory.notifications");
                    // create event handler for notiications
                notificationMsgListener.DataPublisher += NotificationMsgListener_DataPublisher;
                Console.WriteLine("RoadShow Notification Manager started!");
                Sender.Tell("ok");
            });
            Receive<NotificationBusEvent>(x =>
            {
                switch (x.EventId)
                {
                    case "SentNotificationEvent":
                        Console.WriteLine("Supervisoty Notification Manager received SentNotificationEvent");

                        var sentEvent = x.Event;
                        if (sentEvent is SupervisoryMobileNotificationMapper)
                        {
                            Console.Write("Storing Sent Notification event in memory");
                            SupervisoryMobileNotificationMapper map = sentEvent as SupervisoryMobileNotificationMapper;
                          
                            this.SupervisoryMongoStorage.InsertAsync(map).Wait();
                        }
                        break;
                    case "StatusChangeNotificationEvent":
                        Console.WriteLine("Supervisory Notification Manager received StatusChange Event");
                        break;
                }
            });
        }

        private void NotificationMsgListener_DataPublisher(object sender, string e)
        {
            try
            {
                var msg = JsonConvert.DeserializeObject(e, typeof(SupervisoryNotificationMessage));
                notificationMessageBus.Tell(new NotificationBusCommand(new ProcessNotificationCommand(msg, Domain)));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
