﻿using Akka.Actor;
using Insight.Notification.Core.Model;
using Insight.Notification.Domain.Supervisory.Model;
using Insight.Notification.Messaging;
using Insight.Notification.Notifications.Commands;
using System;

namespace Insight.Notification.Domain.Supervisory.Notifications
{
    public class SupervisoryNotificationProcessor : ReceiveActor
    {
        private IActorRef notificationMessageBus;
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public SupervisoryNotificationProcessor(IActorRef notificationMessageBus)
        {
            this.notificationMessageBus = notificationMessageBus;
            Receive<Start>(x =>
            {
                Logger.Info("Starting supervisory notification processor");
                Console.WriteLine("Supervisory Notification Processor Started...");
                //register 
                notificationMessageBus.Tell(new RegisterCommandHandler("ProcessNotificationCommand", Self));
                Sender.Tell("ok");
            });
            Receive<NotificationBusCommand>(x =>
            {
                switch (x.CommandId)
                {
                    case "ProcessNotificationCommand":

                        if (x.Command is SupervisoryNotificationMessage)
                        {
                            Console.WriteLine("Supervisory received notification Msg for processing ");
                            notificationMessageBus.Tell(new NotificationBusCommand(new PublishNotificationCommand(x.Command, SystemDomains.RoadShow)));
                        }
                        break;
                }

            });
        }
    }
}
