﻿using Akka.Actor;
using Insight.Notification.Core.Model;
using Insight.Notification.Domain.Supervisory.Model;
using Insight.Notification.Infra.Services.Insight.Mobile;
using Insight.Notification.Infra.Services.Insight.Mobile.Config;
using Insight.Notification.Infra.Services.Insight.Mobile.Model;
using Insight.Notification.Messaging;
using Insight.Notification.Notifications.Commands;
using Insight.Notification.Notifications.Events;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Insight.Notification.Domain.Supervisory.Notifications
{
    public class SupervisoryNotificationPublisher : ReceiveActor
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private IActorRef notificationMessageBus;
        private NotificationServiceProxy mobileNotificationService;
        private NotificationServiceConfig mobileNSConfig;
        public SupervisoryNotificationPublisher(IActorRef notificationMessageBus)
        {
            this.notificationMessageBus = notificationMessageBus;
            Receive<Start>(x =>
            {
                //"MobileNotificationService"
                Logger.Info("SupervisoryNotificationPublisher Started");
                mobileNSConfig = new NotificationServiceConfig("MobileNotificationService");
                mobileNotificationService = new NotificationServiceProxy(mobileNSConfig);
                //register commands to listen for
                notificationMessageBus.Tell(new RegisterCommandHandler("PublishNotificationCommand", Self));
                Sender.Tell("ok");

            });
            Receive<NotificationBusCommand>(async x =>
            {
                switch (x.CommandId)
                {
                    case "PublishNotificationCommand":
                        await PublishNotification(x);
                        break;
                }
            });
        }

        private async Task<SendNotificationResponse> PublishNotification(NotificationBusCommand cmd)
        {

            var msg = cmd.Command as SupervisoryNotificationMessage;
            NotificationRequest req = await createNotification(msg);
            var result = await mobileNotificationService.SendNotification(req);
            if (result != null)
            {
                if (result.status == "Success")
                {
                    SupervisoryMobileNotificationMapper notificationMappping = new SupervisoryMobileNotificationMapper(msg, req, result as SendNotificationResponse);
                    Logger.Info("Supervisory Received Success Notification for ReqId: {0} with RespID {1}", notificationMappping.SrcReqId, notificationMappping.DistRespId);
                    Console.WriteLine("Supervisory Received Success Notification for ReqId: {0} with RespID {1}", notificationMappping.SrcReqId, notificationMappping.DistRespId);
                    notificationMessageBus.Tell(new NotificationBusEvent(new SentNotificationEvent(notificationMappping, SystemDomains.Supervisory)));
                }
                else
                {
                    Logger.Debug("Supervisory Received {0} when sending notification for ReqId:  ", result.status, msg.Id);
                }
            }
            else
            {
                Logger.Debug("Supervisory publisher received null response");
            }
            return result;
        }
        private async Task<List<string>> getNotificationCoverage()
        {
            List<string> coverageUsers = new List<string>();
            var coverage = await mobileNotificationService.GetUserPreferencesAsync(mobileNSConfig.appCode);
            if (coverage != null)
            {
                foreach (AppPerferenceResponseDetails apd in coverage.response)
                {
                    coverageUsers.Add(apd.userid);
                }
            }
            return coverageUsers;
        }
        private async Task<NotificationRequest> createNotification(SupervisoryNotificationMessage msg)
        {
            var notificaitonMsg = SupervisoryMessageDTO.getAlertMessage(msg);
            Console.WriteLine("Supervisory publisher receive notification msg: " + notificaitonMsg);
            Logger.Info("Supervisory publisher receive notification msg: " + notificaitonMsg);
            List<string> coverageUserIds = await getNotificationCoverage();
            Logger.Info("Supervisory Publisher found coverage for: " + coverageUserIds.ToString());
            NotificationRequest req = new NotificationRequest();
            req.userId = coverageUserIds;
            req.description = notificaitonMsg;
            req.appCode = mobileNSConfig.appCode;
            var details = new NotificationRequestDetails();
            details.alert = notificaitonMsg;
            req.notificationMsg = details;
            return req;
        }

    }
}
