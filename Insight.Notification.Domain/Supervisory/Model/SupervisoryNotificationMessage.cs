﻿using System;
using Insight.Notification.Notifications.Interfaces;
using MongoDB.Bson.Serialization.Attributes;

namespace Insight.Notification.Domain.Supervisory.Model
{
   public class SupervisoryNotificationMessage : INotificationMessage
    {
        string id;
        string domain;
        private DateTime creationTime;
        object payload;

        public SupervisoryNotificationMessage(string id, string domain, object payload)
        {
            creationTime = DateTime.Now;
            this.id = id;
            this.domain = domain;
            this.payload = payload;
        }
        [BsonId]
        public string Id { get => id; }
        [BsonElement]
        public string Domain { get => domain; }
        [BsonElement]
        public DateTime CreationTime { get => creationTime; }
        [BsonElement]
        public object Payload { get => payload; }
    }
}
