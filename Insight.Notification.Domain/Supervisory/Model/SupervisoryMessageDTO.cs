﻿using System;
using System.Collections.Generic;

namespace Insight.Notification.Domain.Supervisory.Model
{
    public class SupervisoryMessageDTO
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static string getAlertMessage(SupervisoryNotificationMessage msg)
        {
            string result = "";

            if (msg.Payload is string)
            {
                return msg.Payload as string;
            }
            if (msg.Payload is SupervisoryPayload)
            {
                var value = msg.Payload as SupervisoryPayload;
                result = value.Message;
            }
            else
            {
                result = findMessageProperty(msg.Payload);
            }
            return result;
        }
        private static string findMessageProperty(object obj)
        {
            List<string> propertyNames = new List<string> { "Message", "message", "Msg", "msg", "AlertMessage", "alertMessage", "alertmessage" };
            string result = "";
            try
            {
                for (int i = 0; i < propertyNames.Count; i++)
                {
                    if (obj.GetType().GetMethod(propertyNames[i]) != null)
                    {
                        var propertyValue = obj.GetType().GetProperty(propertyNames[i]).GetValue(obj);
                        result = propertyValue is string ? propertyValue as string : "";
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Transforming Supervisory Message Error : " + ex.Message);
            }

            return "";
        }


    }
}

