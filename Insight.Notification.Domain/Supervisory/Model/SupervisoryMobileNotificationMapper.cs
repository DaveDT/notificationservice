﻿using Insight.Notification.Infra.Services.Insight.Mobile.Model;

namespace Insight.Notification.Domain.Supervisory.Model
{
    public class SupervisoryMobileNotificationMapper
    {
        public SupervisoryMobileNotificationMapper(SupervisoryNotificationMessage ReqMsg, 
            NotificationRequest mobileSrvReq, SendNotificationResponse mobileSrvRespMsg)
        {
            this.SrcReqId = ReqMsg.Id;
            this.DistRespId = mobileSrvRespMsg.notificationId;
            this.SrcReqMsg = ReqMsg;
            this.DistReqMsg = mobileSrvReq;
            this.DistRespMsg = mobileSrvRespMsg;
        }

        public string SrcReqId { get; set; }
        public string DistRespId { get; set; }
        public SupervisoryNotificationMessage SrcReqMsg { get; set; }
        public NotificationRequest DistReqMsg { get; set; }
        public SendNotificationResponse DistRespMsg { get; set; }

    }
}
