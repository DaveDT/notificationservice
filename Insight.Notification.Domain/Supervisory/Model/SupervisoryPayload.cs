﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Notification.Domain.Supervisory.Model
{
    public class SupervisoryPayload
    {
        public string Message { get; set; }
    }
}
