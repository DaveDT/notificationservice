﻿using Insight.Notification.Domain.Roadshow.Model;
using Insight.Notification.Infra.DB.MongoDB;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Threading;
using System.Threading.Tasks;

namespace Insight.Notification.Domain.Roadshow.Persistence
{

    public class RoadShowMongoDB : IMongoRepository<RoadShowToMobileNotificationMapper>
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private MongoManager<RoadShowToMobileNotificationMapper> MongoStorage;
        public RoadShowMongoDB(string connectionString, string DbName, string collectionName)
        {
            Logger.Info("Create Mongo Manger connectionstring: {0} , DBName: {1}, collectionName: {2} ", connectionString, DbName, collectionName);
            MongoStorage = new MongoManager<RoadShowToMobileNotificationMapper>(connectionString, DbName, collectionName);
        }

        public async Task<DeleteResult> DeleteAsync(string id)
        {
            var filter = Builders<RoadShowToMobileNotificationMapper>.Filter.Where(x => x.SrcReqId == id);
            CancellationToken t = new CancellationToken();
            var result = await this.MongoStorage.Values.DeleteOneAsync(filter);
            return result;
        }

        public async Task<IAsyncCursor<RoadShowToMobileNotificationMapper>> GetAsync()
        {
            var result = await this.MongoStorage.Values.FindAsync<RoadShowToMobileNotificationMapper>(null);

            return result;
        }
        
        public async Task<IAsyncCursor<RoadShowToMobileNotificationMapper>> GetAsync(string id)
        {
             
            var filter = new BsonDocument("SrcReqId ", id);

           var result = await this.MongoStorage.Values.FindAsync<RoadShowToMobileNotificationMapper>(filter);

            return result;
            
        }

        public async Task InsertAsync(RoadShowToMobileNotificationMapper item)
        {
            Logger.Info("Inserting new item into Mongo ReqId: {0} ", item.SrcReqId);
            await this.MongoStorage.Values.InsertOneAsync(item);
        }
    }
}
