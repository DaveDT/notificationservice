﻿using System;
using System.Collections.Generic;

namespace Insight.Notification.Domain.Roadshow.Model
{
    public class RoadShowMeetingPayload 
    {
        public List<string> To { get ; set ; }
        public string Status { get; set ; }
        public string Message { get; set; }
    }
 
}
