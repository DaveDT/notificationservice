﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Insight.Notification.Domain.Roadshow.Model
{
    public static class RoadShowMessageDTO
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static string getAlertMessage(RoadShowNotificationMessage msg)
        {
            string result = "";
          
            if(msg.Payload is string) {
                return msg.Payload as string;
            }
            if(msg.Payload is RoadShowMeetingPayload)
            {
                var value = msg.Payload as RoadShowMeetingPayload;
                result = value.Message;
            }
            else
            {
                result = findMessageProperty(msg.Payload);
            }
            return result;
        }
        private static string findMessageProperty(object obj)
        {
            List<string> propertyNames = new List<string> { "Message", "message", "Msg", "msg", "AlertMessage", "alertMessage", "alertmessage" };
            string result = "";
            try
            {
                for (int i = 0; i < propertyNames.Count; i++)
                {
                    if (obj.GetType().GetMethod(propertyNames[i]) != null)
                    {
                        var propertyValue = obj.GetType().GetProperty(propertyNames[i]).GetValue(obj);
                        result = propertyValue is string ? propertyValue as string : "";
                        break;
                    }
                }
            }catch(Exception ex)
            {
                Logger.Error("Transforming Road Show Message Error : " + ex.Message);
            }

            return "";
        }

        
    }
}
