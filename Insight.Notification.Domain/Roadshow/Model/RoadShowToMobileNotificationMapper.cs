﻿using Insight.Notification.Infra.Services.Insight.Mobile.Model;

namespace Insight.Notification.Domain.Roadshow.Model
{
    public class RoadShowToMobileNotificationMapper
    {
        public RoadShowToMobileNotificationMapper(RoadShowNotificationMessage roadShowReqMsg, NotificationRequest mobileSrvReq, SendNotificationResponse mobileSrvRespMsg)
        {
            this.SrcReqId = roadShowReqMsg.Id;
            this.DistRespId = mobileSrvRespMsg.notificationId;
            this.SrcReqMsg = roadShowReqMsg;
            this.DistReqMsg = mobileSrvReq;
            this.DistRespMsg = mobileSrvRespMsg;
        }

        public string SrcReqId { get; set; }
        public string DistRespId { get; set; }
        public RoadShowNotificationMessage SrcReqMsg { get; set; }
        public NotificationRequest DistReqMsg { get; set; }
        public SendNotificationResponse DistRespMsg { get; set; }


    }
}
