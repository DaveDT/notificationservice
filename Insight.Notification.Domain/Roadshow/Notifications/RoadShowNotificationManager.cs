﻿using Akka.Actor;
using Insight.Notification.Core.Model;
using Insight.Notification.Domain.Roadshow.Model;
using Insight.Notification.Domain.Roadshow.Persistence;
using Insight.Notification.Infra.Messaging.ActiveMQ;
using Insight.Notification.Messaging;
using Insight.Notification.Notifications;
using Insight.Notification.Notifications.Commands;
using Insight.Notification.Notifications.Events;
using Newtonsoft.Json;
using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Threading.Tasks;

namespace Insight.Notification.Domain.Roadshow.Notifications
{
    public class RoadShowNotificationManager : ReceiveActor
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private IActorRef notificationProcessor, notificationPublisher;
        InMemoryNotificationStorage<RoadShowToMobileNotificationMapper> storageMapper;
        private RoadShowMongoDB RoadShowMongoStorage;
        private ActiveMQTopicListener notificationMsgListener;
        private IActorRef notificationMessageBus;
        private ActorSystem system;
        private string Domain; 
        public RoadShowNotificationManager(ActorSystem system, string domain)
        {
            var mongoConfig = ConfigurationManager.GetSection("InsightNotificationsMongoDB") as NameValueCollection;
            this.Domain = domain;
            var mongoConnctionString = mongoConfig["connectionString"];  
            var DbName = mongoConfig["dbName"];
            this.storageMapper = new InMemoryNotificationStorage<RoadShowToMobileNotificationMapper>();
            this.RoadShowMongoStorage = new RoadShowMongoDB(mongoConnctionString, DbName, domain);
            this.system = system;
            
            Receive<Start>(x =>
            {
                this.notificationMessageBus = system.ActorOf<MessageBus<NotificationBusCommand, NotificationBusEvent>>("roadshowNotificationMessagingBus");
                this.notificationProcessor = system.ActorOf(Props.Create(() => new RoadShowNotificationProcessor(notificationMessageBus)), "roadShownotificationProcessor");
                this.notificationPublisher = system.ActorOf(Props.Create(() => new RoadShowNotificationPublisher(notificationMessageBus)), "roadShownotificationPublisher");  
                var t1 = this.notificationProcessor.Ask(new Start());
                var t2 = this.notificationPublisher.Ask(new Start());
                Task.WaitAll(t1,t2);

                // register events
                notificationMessageBus.Tell(new RegisterEventHandler("SentNotificationEvent", Self));
                notificationMessageBus.Tell(new RegisterEventHandler("StatusChangeNotificationEvent", Self));
                notificationMsgListener = new ActiveMQTopicListener("roadShow.notifications");
                // create event handler for notiications
                notificationMsgListener.DataPublisher += NotificationMsgListener_DataPublisher;
                Console.WriteLine("RoadShow Notification Manager started!");
                Sender.Tell("ok");
            });
            Receive<NotificationBusEvent>(x =>
            {
                switch (x.EventId)
                {
                    case "SentNotificationEvent":
                        Console.WriteLine("Notification Manager received SentNotificationEvent");

                        var sentEvent = x.Event;
                        if(sentEvent is RoadShowToMobileNotificationMapper)
                        {
                            Console.Write("Storing Sent Notification event in memory");
                            RoadShowToMobileNotificationMapper map = sentEvent as RoadShowToMobileNotificationMapper;
                            storageMapper.Save(map);
                            this.RoadShowMongoStorage.InsertAsync(map).Wait();
                        }       
                        break;
                    case "StatusChangeNotificationEvent":
                        Console.WriteLine("Notification Manager received SentNotificationEvent");
                        break;
                }
            });
        }

        private void NotificationMsgListener_DataPublisher(object sender, string e)
        {
            try
            {
                var msg = JsonConvert.DeserializeObject(e, typeof(RoadShowNotificationMessage));
                notificationMessageBus.Tell(new NotificationBusCommand(new ProcessNotificationCommand(msg, Domain)));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
