﻿using Akka.Actor;
using Insight.Notification.Core.Model;
using Insight.Notification.Domain.Roadshow.Model;
using Insight.Notification.Messaging;
using Insight.Notification.Notifications.Commands;
using System;

namespace Insight.Notification.Domain.Roadshow.Notifications
{
    public class RoadShowNotificationProcessor : ReceiveActor
    {
        private IActorRef notificationMessageBus;
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public RoadShowNotificationProcessor(IActorRef notificationMessageBus)
        {
            this.notificationMessageBus = notificationMessageBus;
                Receive<Start>(x =>
                {
                    Logger.Info("Starting Road Show notification processor");
                    Console.WriteLine("Road Notification Processor Started...");
                    //register 
                    notificationMessageBus.Tell(new RegisterCommandHandler("ProcessNotificationCommand", Self));
                    Sender.Tell("ok");
                });
            Receive<NotificationBusCommand>(x =>
            {
                switch (x.CommandId)
                {
                    case "ProcessNotificationCommand":
                     
                        if (x.Command is RoadShowNotificationMessage)
                        {
                            Console.WriteLine("Road Show received notification Msg for processing ");
                            notificationMessageBus.Tell(new NotificationBusCommand(new PublishNotificationCommand(x.Command, SystemDomains.RoadShow)));
                        }
                        break;
                }
                    
            });

        }
       
    }
}
