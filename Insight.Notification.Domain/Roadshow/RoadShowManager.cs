﻿using Akka.Actor;
using Insight.Notification.Core.Model;
using Insight.Notification.Domain.Roadshow.Notifications;
using Insight.Notification.Interests;
using Insight.Notification.Messaging;
using Insight.Notification.System.Commands;
using System;
using System.Threading.Tasks;

namespace Insight.Notification.Domain.Roadshow
{
    public class RoadShowManager: ReceiveActor
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private ActorSystem system;
        private IActorRef systemMessageBus;
        private IActorRef notificationManager;
        private string Domain = SystemDomains.RoadShow;
        public RoadShowManager(ActorSystem system, IActorRef systemMessageBus)
        {
            this.system = system;
            this.systemMessageBus = systemMessageBus;
            Receive<Start>(x =>
            {
                Logger.Info("RoadShow Manager Started ");
                 Console.WriteLine("RoadShow Manager Started!");

                 systemMessageBus.Tell(new RegisterCommandHandler("ResetCommand", Self));
                 this.notificationManager = system.ActorOf(Props.Create(() => new RoadShowNotificationManager(system, SystemDomains.RoadShow)), "roadshowNotificationManager");
                 var t0 = notificationManager.Ask(new Start());
                 Task.WaitAll(t0);
                 Sender.Tell("ok");
            });
            Receive<SystemBusCommand>(x =>
            {
                if (x.CommandId == "ResetCommand")
                    Console.WriteLine("RoadShow Manager Received ResetCommand");
            });

        }
        protected override void PostStop()
        {
            
            base.PostStop();
        }
        
    }
}
